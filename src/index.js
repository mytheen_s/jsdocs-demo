/**
 *
 * @param {*} _text this function returns a paragraph element
 */
const ParagraphElement = (_text) => {
  return `<p>${_text}</p>`;
};
